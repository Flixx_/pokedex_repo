# Pokedex

The website lists the pokemons of the pokemons.json.

You can like pokemons and filter them by liked pokemons.
You can also search them by name, or by type. The search by level is disabled because I didn't understand in which way I could add the level to the pokemons.
By going on the '+' button you can see more about the pokemon, with his stats.

## Technologies used

I use react for the development of the app, netlify for online deployement, and firebase for online database.
The website is also accessible for mobile.

**Links**

- [git](https://bitbucket.org/Flixx_/pokedex_repo/src/)
- [site](https://laforge-tech-test.netlify.app)