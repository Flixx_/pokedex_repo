import React from 'react';
import './App.css';
import SearchBox from './components/SearchBox';
import PokemonList from './components/PokemonList'
import data from './pokemons.json';
import {db} from './config.js';

class App extends React.Component {
  componentDidMount(){
    document.title = "Pokedex";
    const state = [...this.state.pokemons]
    db.ref('liked').on('value', snap => {
      snap.forEach(child => {
        const index = state.findIndex(item => item.id === child.val())
        state[index].liked = 'liked'
        this.setState({state})
      })
    });
  }

  constructor(props) {
    super(props)
    
    this.state =  {
      likedFilter: false,
      pokemons: data.map(data => {
        return ({
          id: data.id,
          name: data.name,
          stats: data.stats,
          type: data.type.map((item) => { return (item) }),
          img: data.img,
          level: '',
          liked: 'notliked',
          showModal: false
        })
      }),
      searchName: '',
      searchType: '',
      searchLevel: '',
    }
    this.handleClick = this.handleClick.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }


  handleOpenModal = (event) => {
    const state = [...this.state.pokemons]
    const index = state.findIndex(item => item.id === event.target.id)
    state[index].showModal = true
    this.setState({state})
  }

  handleCloseModal = (event) => {
    const state = [...this.state.pokemons]
    const index = state.findIndex(item => item.id === event.target.id)
    state[index].showModal = false
    this.setState({state})
  }

  handleClick = (event) => {
    event.preventDefault()

    if (event.target.className === 'pok-info-notliked'){
      const state = [...this.state.pokemons]
      const index = state.findIndex(item => item.id === event.target.id)
      state[index].liked = 'liked'
      this.setState({state})
      db.ref('liked').push(event.target.id)

    } else {
      const state = [...this.state.pokemons]
      const index = state.findIndex(item => item.id === event.target.id)
      state[index].liked = 'notliked'
      this.setState({state})
      let key
       db.ref('liked').once('value', snap => {
          snap.forEach ( (child) => {     
            if (child.val() === event.target.id) {
              key = child.key
              return(child.key)
            }
          });
        });
        
      db.ref('liked').child(key).remove()
    }
  }
  
  handleLikeClick = (event) => {
    if (event.target.value === 'pokedex') {
      this.setState(state => ({ likedFilter: false }))
      event.target.className = 'button-title'
    } else {
      this.setState(state => ({ likedFilter: true }))
      event.target.className = 'button-title-selected'
    }
  }
  
  editSearchNameTerm = (event) => {
    this.setState({ searchName: event.target.value })
  }

  editSearchTypeTerm = (event) => {
    this.setState({searchType: event.target.value})
  }

  editSearchLevelTerm = (event) => {
    this.setState({searchLevel: event.target.value})
  }


  render() {
    const { likedFilter, pokemons, searchName, searchType } = this.state
    var filteredPokemons = pokemons
    if (likedFilter === true) {
      filteredPokemons = filteredPokemons.filter(item => (
        item.liked === 'liked'
      ))
    }
    filteredPokemons = filteredPokemons.filter(item => (
      item.name.toLowerCase().includes(searchName.toLowerCase())
    ))
      
    filteredPokemons = filteredPokemons.filter(data => (
      data.type.filter(item => (item.toLowerCase().includes(searchType.toLowerCase()))).length > 0
    ))
  
    

    return (
      <div id='root'>
        <div className='header'>
          <button className={likedFilter ? 'button-title' : 'button-title-selected'} onClick={e => this.handleLikeClick(e, "value")} value={'pokedex'}>POKEDEX</button>
          <button className={likedFilter ? 'button-title-selected' : 'button-title'} onClick={e => this.handleLikeClick(e, "value")} value={'liked'}>LIKED</button>
        </div> 
        <div className='search'>
          <SearchBox className='searchbar'  placeholder='Search' onChange={this.editSearchNameTerm}/>
          <SearchBox className='search-type'  placeholder='Type' onChange={this.editSearchTypeTerm}/>
          <SearchBox className='search-level'  placeholder='Level' onChange={this.editSearchLevelTerm}/>
        </div>
        <PokemonList data={filteredPokemons} onClick={this.handleClick} handleCloseModal={this.handleCloseModal} handleOpenModal={this.handleOpenModal} />
      </div>
    )}
}



export default App;