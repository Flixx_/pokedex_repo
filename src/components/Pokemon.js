import React from 'react';
import '../App.css';

const Pokemon = ({ data, onClick, handleCloseModal, handleOpenModal }) => {
  
  var type = data.type.map((item, index) => { return (<div className={'pok-info-type ' + item.toLowerCase()} key={index}>{item}</div>) })
  let modal = <div></div>
  
  if (data.showModal) {
    modal = (
      <div className='pok-modal-background'>
        <div className='pok-modal'>
          <div className='pok-modal-title'>
            <img className='img-data' src={data.img} alt='img' />
            <button className='pok-info-buttonModal' onClick={handleCloseModal} id={data.id}></button>
          </div>
          <div className='pok-title'>
            <p className='pok-info-id'>{data.id}</p>
            <p className='pok-info-name'>{data.name.toUpperCase()}</p>
          </div>
          <div className='pok-type'>
            {type}
          </div>
          <div className='pok-info-stats'>
              <div className='modal-icone-hp'></div><p>{data.stats.hp}</p>
              <div className='modal-icone-attack'></div><p>{data.stats.attack}</p>
              <div className='modal-icone-defense'></div><p>{data.stats.defense}</p>
              <div className='modal-icone-spattack'></div><p>{data.stats.spattack}</p>
              <div className='modal-icone-spdefense'></div><p>{data.stats.spdefense}</p>
              <div className='modal-icone-speed'></div><p>{data.stats.speed}</p>
          </div>
          </div>
      </div>
    )
  }

  return (
    <div>
      <div className='data'>
        <img className='img-data' src={data.img} alt='img' />
        <div className='pok-infos'>
          <div className='pok-title'>
            <p className='pok-info-id'>{data.id}</p>
            <p className='pok-info-name'>{data.name.toUpperCase()}</p>
          </div>
          <div className='pok-type'>
            {type}
          </div>
            <button className='pok-info-more' onClick={handleOpenModal} id={data.id}></button>
            <button className={"pok-info-" + data.liked} onClick={onClick} id={data.id}></button>
        </div>
      </div >
      {modal}
    </div>
  )
}

export default Pokemon;