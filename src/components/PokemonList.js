import React from 'react';
import '../App.css';
import Pokemon from './Pokemon'

const PokemonList = ({ data, onClick, handleCloseModal, handleOpenModal}) => {
  
  return (
    <div className='data-container'>
      {
        data.map((pokemon, index) => {
          
          return (
            <Pokemon key={index} data={pokemon} onClick={onClick} handleCloseModal={handleCloseModal} handleOpenModal={handleOpenModal}/> 
        )})
      }
    </div>
  )
}

export default PokemonList;