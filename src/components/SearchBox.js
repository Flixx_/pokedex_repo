import React from 'react';
import '../App.css'

const SearchBox = ({placeholder,onChange, className}) =>{
    return(
        <input type='search'
        className={className}
        placeholder={placeholder}
        onChange = {onChange}
        />
    )
}

export default SearchBox;