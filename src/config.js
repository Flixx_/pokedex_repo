import Firebase from 'firebase'

var firebaseConfig = {
    apiKey: "AIzaSyC42gk49z9SXYBg4WBN5ltgmOEKtGHb2mc",
    authDomain: "pokedex-2271b.firebaseapp.com",
    databaseURL: "https://pokedex-2271b-default-rtdb.firebaseio.com",
    projectId: "pokedex-2271b",
    storageBucket: "pokedex-2271b.appspot.com",
    messagingSenderId: "5652497368",
    appId: "1:5652497368:web:39c016b74c488fc5ba68ad",
    measurementId: "G-N98HK2Z967"
  };

const app = Firebase.initializeApp(firebaseConfig);
export const db = app.database();